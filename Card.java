public class Card{
	private String suit;
	private String number;
	
	public Card(String suit, String number){
		this.suit = suit;
		this.number = number;
	}
	
	//getters
	public String getSuit(){
		return this.suit;
	}
	public String getNumber(){
		return this.number;
	}
	
	//toString
	public String toString(){
		return this.number + " of " + this.suit;
	}
}