public class GameManager{
	
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;	
	
	public GameManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	public Deck getDrawPile(){
		return this.drawPile;
	}
	
	public Card getCenterCard(){
		return this.centerCard;
	}
	
	public Card getPlayerCard(){
		return this.playerCard;
	}
	
	public int getNumberOfCards(){
		return this.drawPile.getNumberOfCards();
	}
		
	public void dealCards(){
		this.drawPile.shuffle();
		
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}	
	
	public int calculatePoints(){
		if(this.centerCard.getNumber() == this.playerCard.getNumber()){
			return 4;
		} else if(this.centerCard.getSuit() == this.playerCard.getSuit()){
			return 2;
		} else {
			return -1;
		}
	}
	
	/*
	
	*/
	public String toString(){
		String output = 
					"-----------------------------------" + "\n"
					+ "|  Center Card  | |  Player Card  |\n" 
					+ "----------------------------------- \n";
		
		String b = "\u001B[30m";
		String w = "\u001B[37m";	
		String r = "\u001B[31m";	
		String bb = "\u001B[40m";
		String wb = "\u001B[47m";
		String playerCardColour = b;
		String centerCardColour = b;
		
		int cardHeight = 13; 

		String[] rows = new String[cardHeight];
		
		for(int i = 0; i < cardHeight; i++){								
			if(this.centerCard.getSuit().equals("HEARTS") || this.centerCard.getSuit().equals("DIAMONDS"))
				centerCardColour = r;
			else 
				centerCardColour = b;	
			
			if(this.playerCard.getSuit().equals("HEARTS") || this.playerCard.getSuit().equals("DIAMONDS"))
				playerCardColour = r;
			else 
				playerCardColour = b;	
		
			if(i == cardHeight/2)
				rows[i] += " " + wb + w + centerCardColour + " " + this.centerCard + " " + bb + " " + wb + " " + playerCardColour + this.playerCard + " " + bb;
			else
				rows[i] += " " + wb + w + " " + this.centerCard + " " + bb + " " + wb + " " + this.playerCard + " " + bb;							
		}		
		
		for(int i = 0; i < cardHeight; i++){
			output += rows[i].substring(4) + "\n";
		}			
		
		output += "-----------------------------------" + "\n";	
		return output;				
	}
}