import java.util.Scanner;

public class LuckyCardGameApp{
	public static void main(String[] args){
		//The scanner let's the user control when to start the game and start the next round 
		Scanner scan = new Scanner(System.in);
		
		GameManager manager = new GameManager();
		
		System.out.println("\nHello player\n");
		
		System.out.println("Here are the rules : ");
		System.out.println("""
							*********************************************************************************************************
							|	1. At the start of the game, there is a deck of 52 cards.                                 	|
							|	2. You must get 5 points before the draw pile runs out of cards.                          	|
							|	3. At each round, two cards are drawn from the draw pile a center card and a player card. 	|
							|	4. Your score gets updated based on comparing the two cards as follows:               		|
							|	    a. If the value of the two cards matches,\u001B[32m you gets 4 points.\u001B[37m                          	|
							|	    b. If the suit of the two cards matches,\u001B[32m you gets 2 points.\u001B[37m                           	|
							|	    c. If neither value nor suit matches,\u001B[31m you lose a point.\u001B[37m                                	|
							|	5. You win if you have achieved\u001B[32m 5 or more points\u001B[37m before the deck runs out of cards.       	|
							*********************************************************************************************************
						""");		
		
		System.out.println("[Press enter to begin]");		
		scan.nextLine();
	
		int totalPoints = 0;
	
		int round = 1;
	
		while(totalPoints < 5 && manager.getNumberOfCards() > 0){
			System.out.println("This is round: " + round);
			
			if(manager.getNumberOfCards() >= 2 && round != 1)
				manager.dealCards();
			
			System.out.println(manager);
			
			totalPoints += manager.calculatePoints();
			
			if(manager.calculatePoints() == 4)
				System.out.println("=====>\u001B[32m THE VALUES MATCH (+4)\u001B[37m <=====\n");
			else if(manager.calculatePoints() == 2)
				System.out.println("=====>\u001B[32m THE SUITS MATCH (+2)\u001B[37m <=====\n");
			else
				System.out.println("=====>\u001B[31m NOTHING MATCHES (-1)\u001B[37m <=====\n");			
			
			if(totalPoints < 0)
				System.out.println("You have " + "\u001B[31m" + totalPoints + " points \n" + "\u001B[37m");
			else if (totalPoints > 0)
				System.out.println("You have " + "\u001B[32m" + totalPoints + " points \n" + "\u001B[37m");
			else
				System.out.println("You have " + totalPoints + " points \n");
			
			System.out.println("There are " + manager.getNumberOfCards() + " cards left in the deck\n");
			
			if(totalPoints >= 5 || manager.getNumberOfCards() <= 1)
				System.out.println("[Press enter to end the game]");		
			else		
				System.out.println("[Press enter to start the next round]");		
			scan.nextLine();
			
			System.out.println("************************************** \n ");					
			
			round++;
		}
		
		System.out.println("You have " + totalPoints + " points and there are "+ manager.getNumberOfCards() + " cards left in the deck");	
		
		if(totalPoints < 5)
			System.out.println("You lost");
		else
			System.out.println("You won");	
	}
}